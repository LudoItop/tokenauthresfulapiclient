appControllers.controller('CourseListCtrl', ['$scope', '$sce', 'CourseService',
    function CourseListCtrl($scope, $sce, CourseService) {

        $scope.courses = [];

        CourseService.findAllPublished().success(function(data) {
            for (var courseKey in data) {
                data[courseKey].content = $sce.trustAsHtml(data[courseKey].content);
            }

            $scope.courses = data;
        }).error(function(data, status) {
            console.log(status);
            console.log(data);
        });
    }
]);

appControllers.controller('CourseViewCtrl', ['$scope', '$routeParams', '$location', '$sce', 'CourseService',
    function CourseViewCtrl($scope, $routeParams, $location, $sce, CourseService) {

        $scope.course = {};
        var id = $routeParams.id;



        CourseService.read(id).success(function(data) {

            data.content = $sce.trustAsHtml(data.content);
            $scope.course = data;
        }).error(function(data, status) {
            console.log(status);
            console.log(data);
        });

    }
]);


appControllers.controller('AdminCourseListCtrl', ['$scope', 'CourseService',
    function AdminCourseListCtrl($scope, CourseService) {
        $scope.courses = [];

        CourseService.findAll().success(function(data) {
            $scope.courses = data;
        });


    }
]);

appControllers.controller('AdminCourseCreateCtrl', ['$scope', '$location', 'CourseService',
    function AdminCourseCreateCtrl($scope, $location, CourseService) {

        $('#textareaContent').wysihtml5({"font-styles": false});

        $scope.save = function save(course, shouldPublish) {
            if (course != undefined
                && course.title != undefined
                ) {

                var content = $('#textareaContent').val();
                if (content != undefined) {
                    course.content = content;

                    if (shouldPublish != undefined && shouldPublish == true) {
                        course.is_published = true;
                    } else {
                        course.is_published = true;
                    }



                    CourseService.create(course).success(function(data) {
                        $location.path("/admin");
                    }).error(function(status, data) {
                        console.log(status);
                        console.log(data);
                    });
                }
            }
        }
    }
]);

appControllers.controller('AdminCourseEditCtrl', ['$scope', '$routeParams', '$location', '$sce', 'CourseService',
    function AdminCourseEditCtrl($scope, $routeParams, $location, $sce, CourseService) {
        $scope.course = {};
        var id = $routeParams.id;

        CourseService.read(id).success(function(data) {
            $scope.course = data;
            $('#textareaContent').wysihtml5({"font-styles": false});
            $('#textareaContent').val($sce.trustAsHtml(data.content));
        }).error(function(status, data) {
            $location.path("/admin");
        });

        $scope.save = function save(course, shouldPublish) {
            if (course !== undefined
                && course.title !== undefined && course.title != "") {

                var content = $('#textareaContent').val();
                if (content !== undefined && content != "") {
                    course.content = content;

                    if (shouldPublish != undefined && shouldPublish == true) {
                        course.is_published = true;
                    } else {
                        course.is_published = false;
                    }

                    // string comma separated to array
                    if (Object.prototype.toString.call(course.tags) !== '[object Array]') {
                        course.tags = course.tags.split(',');
                    }

                    CourseService.update(course).success(function(data) {
                        $location.path("/admin");
                    }).error(function(status, data) {
                        console.log(status);
                        console.log(data);
                    });
                }
            }
        }
    }
]);

appControllers.controller('AdminUserCtrl', ['$scope', '$location', '$window', 'UserService', 'AuthenticationService',
    function AdminUserCtrl($scope, $location, $window, UserService, AuthenticationService) {

        //Admin User Controller (signIn, logOut)
        $scope.signIn = function signIn(username, password) {
            if (username != null && password != null) {

                UserService.signIn(username, password).success(function(data) {
                    AuthenticationService.changeAuth(true);
                    $window.sessionStorage.token = data.token;
                    $location.path("/admin");
                }).error(function(status, data) {
                    console.log(status);
                    console.log(data);
                });
            }
        }

        $scope.logOut = function logOut() {
            if (AuthenticationService.isAuthenticated) {

                UserService.logOut().success(function(data) {
                    AuthenticationService.changeAuth(false);
                    delete $window.sessionStorage.token;
                    $location.path("/");
                }).error(function(status, data) {
                    console.log(status);
                    console.log(data);
                });
            }
            else {
                $location.path("/admin/login");
            }
        }

        $scope.register = function register(username, password, passwordConfirm) {

            if (AuthenticationService.isAuthenticated) {
                $location.path("/admin");
            }
            else {
                UserService.register(username, password, passwordConfirm).success(function(data) {
                    $location.path("/admin/login");
                }).error(function(status, data) {
                    console.log(status);
                    console.log(data);
                });
            }
        }
    }
]);


appControllers.controller('CourseListTagCtrl', ['$scope', '$routeParams', '$sce', 'CourseService',
    function CourseListTagCtrl($scope, $routeParams, $sce, CourseService) {

        $scope.courses = [];
        var tagName = $routeParams.tagName;

        CourseService.findByTag(tagName).success(function(data) {
            for (var courseKey in data) {
                data[courseKey].content = $sce.trustAsHtml(data[courseKey].content);
            }
            $scope.courses = data;
        }).error(function(status, data) {
            console.log(status);
            console.log(data);
        });

    }
]);

appControllers.controller('NavbarCtrl', ['$scope', '$routeParams', '$sce', 'CourseService', 'AuthenticationService',
    function NavbarCtrl($scope, $routeParams, $sce, CourseService, AuthenticationService) {

        function menu()
        {
            if (AuthenticationService.isAuthenticated) {
                var menu = [{
                    "title": "logout",
                    "link": "admin/logout"
                },{
                    "title": "admin",
                    "link": "admin"
                }];

            }
            else
            {
                var menu = [{
                    "title": "login",
                    "link": "admin/login"
                }];

            }
            return menu;
        }


        AuthenticationService.registerObserverCallback(function()
        {
            $scope.menu = menu();
        });

        console.log(menu);

        $scope.menu = menu();

    }
]);
