'use strict';

var app = angular.module('app', ['ngRoute', 'appControllers', 'appServices', 'appDirectives']);

var appServices = angular.module('appServices', []);
var appControllers = angular.module('appControllers', []);
var appDirectives = angular.module('appDirectives', []);

var options = {};
options.api = {};
options.api.base_url = "http://localhost:3001";


app.config(['$locationProvider', '$routeProvider',
    function($location, $routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'partials/course.list.html',
                controller: 'CourseListCtrl'
            }).
            when('/course/:id', {
                templateUrl: 'partials/course.view.html',
                controller: 'CourseViewCtrl'
            }).
            when('/tag/:tagName', {
                templateUrl: 'partials/course.list.html',
                controller: 'CourseListTagCtrl'
            }).
            when('/admin', {
                templateUrl: 'partials/admin.course.list.html',
                controller: 'AdminCourseListCtrl',
                access: { requiredAuthentication: true }
            }).
            when('/admin/course/create', {
                templateUrl: 'partials/admin.course.create.html',
                controller: 'AdminCourseCreateCtrl',
                access: { requiredAuthentication: true }
            }).
            when('/admin/course/edit/:id', {
                templateUrl: 'partials/admin.course.edit.html',
                controller: 'AdminCourseEditCtrl',
                access: { requiredAuthentication: true }
            }).
            when('/admin/register', {
                templateUrl: 'partials/admin.register.html',
                controller: 'AdminUserCtrl'
            }).
            when('/admin/login', {
                templateUrl: 'partials/admin.signin.html',
                controller: 'AdminUserCtrl'
            }).
            when('/admin/logout', {
                templateUrl: 'partials/admin.logout.html',
                controller: 'AdminUserCtrl',
                access: { requiredAuthentication: true }
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);


app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
});

app.run(function($rootScope, $location, $window, AuthenticationService) {
    $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
        //redirect only if both isAuthenticated is false and no token is set
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
            && !AuthenticationService.isAuthenticated && !$window.sessionStorage.token) {

            $location.path("/admin/login");
        }
    });
});
